# Artzoom

## What is it?
Artzoom is a screen magnifier application that will stretch the screen horizontally and vertically to fit your needs. Its primary use is to create pixel art in your favorite editor when your project requires nonsquare pixel sizes, for example, sprite artwork for computers like the Tandy Color Computer, Commodore 64, some older CGA and EGA resolutions, and others.

## TODO Notices
This program may not work with multimonitor setups properly. This will be fixed soon, but it's safe to give it a try.

## Installation
Download the latest version from: https://gitlab.com/trs-eric/art-zoom/-/tree/main/release

Just unzip and run.

## Usage
Art Zoom consists of the main window which allow you to move and resize it, a toolbar, a viewport, and a status bar.

Along the toolbar are the following buttons:
- *Move Magnifier* - When not following the mouse, this button allows you to position the viewport of the application. Press escape once you have positioned the viewport with your mouse. You can also press Esc as a shortcut to begin moving the viewport.
- *Follow Mouse* - Turns on and off the ability of the viewport to follow the mouse.
- *Zoom In* - Zooms the viewport in.
- *Zoom Out* - Zooms the viewport out.
- *Adjust Horizontal Ratio* - Sets the horizontal viewing ratio. You can choose any value from greater than 0 to infinity. You can also use fractions of a number in decimal form, for example, you can set the viewport to 1.66:1. Please be aware that Art Zoom assumes your actual monitor's pixels are square. Future enhancements may correct this. Other common aspect ratios are available here: https://en.wikipedia.org/wiki/Aspect_ratio_%28image%29
- *Adjust Vertical Ratio* - Sets the vertical viewing ratio similar to the horizontal ratio.
- *Always On Top* - Keeps Art Zoom on top of all other windows.
- *About* - You will find my contact information here.

Artzoom also contains a config.ini file you can adjust the above settings by default to suit your purposes.

## Support and Feature Requests
Reach out to me at my email address visible in the About section of the application or via Gitlab's features.

## Authors and acknowledgment
This program is written and maintained by Eric Canales.

Special thanks to Franco and Pupil who provided a great starting point.

Icons are selected from Silk icon set 1.3, provided under Creative Commons license by Mark James.

## License
Art Zoom
Copyright (C) 2022 Eric Canales

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Iconography License

This software makes use of artwork available under a different license.

Silk icon set 1.3

_________________________________________
Mark James
http://www.famfamfam.com/lab/icons/silk/
_________________________________________

This work is licensed under a
Creative Commons Attribution 2.5 License.
[ http://creativecommons.org/licenses/by/2.5/ ]

This means you may use it for any purpose,
and make any changes you like.
All I ask is that you include a link back
to this page in your credits.

Are you using this icon set? Send me an email
(including a link or picture if available) to
mjames@gmail.com

Any other questions about this icon set please
contact mjames@gmail.com
